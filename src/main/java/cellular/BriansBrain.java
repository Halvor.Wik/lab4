package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid brainGrid;

    public BriansBrain(int row, int col) {
        brainGrid = new CellGrid(row, col, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return brainGrid.get(row,column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < brainGrid.numRows(); row++) {
            for (int col = 0; col < brainGrid.numColumns(); col++) {
                if (random.nextBoolean()) {
                    brainGrid.set(row, col, CellState.ALIVE);
                } else {
                    brainGrid.set(row, col, CellState.DEAD);
                }
            }
        }
    }


    @Override
    public void step() {
        IGrid nextGeneration = brainGrid.copy();
        for (int row = 0; row < numberOfColumns(); row++) {
            for (int col = 0; col < numberOfColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        brainGrid = nextGeneration;

    }

    @Override
    public CellState getNextCell(int row, int col) {
        int aliveCellNeighbors = countNeighbors(row, col, CellState.ALIVE);

        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        if (getCellState(row, col) == CellState.DYING) {
            return CellState.DEAD;
        }
        if (getCellState(row, col) == CellState.DEAD) {
            if (aliveCellNeighbors == 2) {
                return  CellState.ALIVE;
            }
            else {
                return CellState.DEAD;
            }
        }
        return null;
    }

    @Override
    public int numberOfRows() {
        return brainGrid.numRows();
    }

    @Override
    public int numberOfColumns() {
        return brainGrid.numColumns();
    }

    private int countNeighbors(int row, int col, CellState state) {
        int neighbors = 0;
        for (int x = row - 1; x <= row + 1; x++) {
            for (int y = col -1; y <= col + 1; y++) {

                if (x == row && y == col)
                    continue;

                if (x < 0 || y < 0)
                    continue;

                if(x >= brainGrid.numRows())
                    continue;

                if(y >= brainGrid.numColumns())
                    continue;

                if (state == brainGrid.get(x, y)) {
                    neighbors ++;

                }
            }
        }
        return neighbors;
    }

    @Override
    public IGrid getGrid() {
        return brainGrid;
    }
}
