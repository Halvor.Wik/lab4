package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    CellState initialState = CellState.DEAD;
    ArrayList<CellState> grid = new ArrayList<>();

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.initialState = initialState;
        grid = new ArrayList<CellState>();
        for (int row = 0; row < rows; row++ ) {
            for (int col = 0; col < cols; col++) {
                grid.add(initialState);
            }
        }
	}

    private int indexOf(int row, int col) {
        if (row < 0 || row >= rows) {
            throw new IndexOutOfBoundsException("Illegal row value");
        }
        if (col < 0 || col >= cols) {
            throw new IndexOutOfBoundsException("Illegal col value");
        }
        return col+row*cols;
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
         grid.set(indexOf(row,column), element);
    }

    @Override
    public CellState get(int row, int column) {
        return grid.get(indexOf(row, column));
    }

    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.DEAD);

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                newGrid.set(row, col, get(row, col));
            }
        }
        return newGrid;
    }
    
}
